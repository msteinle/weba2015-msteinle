function varianteAsynchrone() {
    executerVarianteAvecJQuery(true);
}

function varianteSynchrone() {
    executerVarianteAvecJQuery(false);
}

function executerVarianteAvecJQuery(async) {

    // Initialisation d'une variable
    var maVariable = 0;
    console.debug("Initialisation [" + maVariable + "]");

    // Appeler le serveur à l'aide de l'objet XmlHttpRequest (via JQuery)
    console.debug("Envoi requête [" + maVariable + "]");
    jQuery.ajax({
        url: "info_serveur.txt",
        async: async,
        success: function (result) {
            // Le paramètre "result" contient la réponse ("1" dans notre exemple)
            // Mettre à jour la variable une fois la réponse reçue
            maVariable = result;
            console.debug("Réponse reçue [" + maVariable + "]");
        }
    });

    // Afficher la variable
    console.debug("Affichage final [" + maVariable + "]");
}

function executerVarianteAvecJavaScriptPur(async) {

    // Initialisation d'une variable
    var maVariable = 0;
    console.debug("Initialisation [" + maVariable + "]");


    // préparation de l'objet XHR
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            // Mettre à jour la variable une fois la réponse reçue
            // l'attribut "responseText" de l'objet XHR contient la réponse sous forme texte ("1")
            maVariable = xhttp.responseText;
            console.debug("Réponse reçue [" + maVariable + "]");
        }
    };
    xhttp.open("GET", "info_serveur.txt", async);

    // Appeler le serveur à l'aide de l'objet XmlHttpRequest (sans JQuery)
    console.debug("Envoi requête [" + maVariable + "]");
    xhttp.send();

    // Afficher la variable
    console.debug("Affichage final [" + maVariable + "]");
}

