// définitions
function sayHi() {
    alert("Hi!")
}

var sayHello = function () {
    alert("Hello!");
};


// Les noms des fonctions sont des variables comme les autres
sayHi = sayHello;

sayHi(); // va dire "hello"

