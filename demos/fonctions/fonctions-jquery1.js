
function direBonjour() {
    alert('Bonjour!');
}
function sayHello() {
    alert('Hello');
}

// Soit:
$('#rd_unit_us').on('click', sayHello);
// Ou en plus court:
$('#rd_unit_fr').click(direBonjour);


