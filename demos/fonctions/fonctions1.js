// définitions: deux manières
function sayHi() {
    alert("Hi!")
}

var sayHello = function () {
    alert("Hello!");
};

// appels
sayHi();
sayHello();

