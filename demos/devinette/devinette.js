
$(function() {
    $('#guess').find('input').change(
        function () {
            // 'this' est ici l'élément sur lequel l'élément se déclenche (l'input)
            var letter_id = this.id;
            console.debug('letter_id: ' + letter_id);
            if(letterMatches(letter_id)) {
                showLetter(letter_id);
            } else {
                hideLetter(letter_id);
            }
        }
    )
});


function letterMatches(letter_id) {
    return $('#' + letter_id + '_char').text() == $('#' + letter_id).val();
}

function showLetter(letter_id) {
    console.debug('showLetter');
    toggleDisplay(letter_id, 'char', 'question');
}

function hideLetter(letter_id) {
    console.debug('hideLetter');
    toggleDisplay(letter_id, 'question', 'char');
}


function toggleDisplay(letter_id, stateToShow, stateToHide) {
    console.debug('toggleDisplay. show: ' + stateToShow + ' hide: ' + stateToHide);
    $('#' + letter_id + '_' + stateToShow).show();
    $('#' + letter_id + '_' + stateToHide).hide();
}
